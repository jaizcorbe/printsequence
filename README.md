**Prerequisites**
For this exercise you need to have installed and configured git, maven and java 8 of higher
Nice to have a java IDE

**Instructions**
We have a program that gets parameters from the command line and prints sequence numbers.  
We want to improve the code and make it more flexible and easier to maintain.

1. Identify where in the program can produce an error result and handle it in a way that the program can't end in an error
2. Add the fibonacci numbers sequence to be printed
3. Add unit tests to ensure the fibonacci sequence correctly generated
4. Did you need any refactor to test the sequence generation?  Can you identify any object pattern or java interface alike?
5. Add the sequence of alphabet letters to be printed
6. Refactor the code to print the sequences to a file
