package coding.exercise;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        String seqId = args[0];
        int n = Integer.valueOf(args[1]);
        if("ODD".equals(seqId)) {
            printSequence(n,1,2);
        } else if ("EVEN".equals(seqId)){
            printSequence(n,0,2);
        }
    }

    private static void printSequence(int n, int first, int step) {
        int i = 0;
        int current = first;
        while(i<n){
            System.out.println(current);
            current = current + step;
            i++;
        }
    }
}
